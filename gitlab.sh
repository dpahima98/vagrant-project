#!/bin/bash
echo "Installing Requirements"
sudo apt-get update > /dev/null 2>&1
sudo apt upgrade -y > /dev/null 2>&1
sudo apt-get install -y ca-certificates curl openssh-server > /dev/null 2>&1

echo "Installing GitLab CE"
curl -sS https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.deb.sh | sudo bash
sudo apt-get update > /dev/null 2>&1
sudo apt-get install -y gitlab-ce > /dev/null 2>&1
sudo gitlab-ctl reconfigure 
sudo gitlab-ctl status

echo "Installing GitLab Multi Runner"
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-ci-multi-runner/script.deb.sh | sudo bash
sudo apt-get install -y gitlab-ci-multi-runner

sleep 30s

echo "Browse to the hostname and login"
GITLABPWD=$(sudo cat /etc/gitlab/initial_root_password | grep ^Password)
echo $GITLABPWD
